import React from 'react';
import ReactDOM from 'react-dom';
import Grid from "./components/grid/Grid";
import {Provider} from "react-redux";
import store from "./store";

ReactDOM.render(
    <Provider store={store}>
        <Grid/>
    </Provider>,
    document.getElementById('root')
);
