import React from 'react';
import PropTypes from 'prop-types';


export default class DragableItem extends React.Component {

    constructor(props) {
        super(props);

        this.element = React.createRef();
    }

    componentDidMount() {
        this.element.current.addEventListener('mousedown', this.onDragStart, false);
    }

    componentWillUnmount() {
        window.removeEventListener('mousemove', this.onDrag, false);
        window.removeEventListener('mouseup', this.onDragEnd, false);

        this.onDragStart = null;
        this.onDrag = null;
        this.onDragEnd = null;
    }

    onDragStart = e => {
        e.preventDefault();
        e.stopPropagation();

        window.dispatchEvent(new CustomEvent("activateDroppableArea"));

        this.startY = parseInt(this.element.current.getAttribute('y'));
        this.startX = parseInt(this.element.current.getAttribute('x'));

        this.diffY = e.y;
        this.diffX = e.x;

        window.addEventListener('mousemove', this.onDrag, false);
        window.addEventListener('mouseup', this.onDragEnd, false);

        if (typeof this.props.onDragStart === 'function') {
            this.props.onDragStart(e, {x: this.startX, y: this.startY});
        }
    };

    onDrag = e => {
        let diffX = e.x - this.diffX;
        let diffY = e.y - this.diffY;

        if (typeof this.props.onDrag === 'function') {
            this.props.onDrag(e, {x: this.startX + diffX, y: this.startY + diffY});
        }
    };

    onDragEnd = e => {
        const event = new CustomEvent("dropitem");
        event.data = this.props.data;

        window.dispatchEvent(event);
        window.removeEventListener('mousemove', this.onDrag, false);
        window.removeEventListener('mouseup', this.onDragEnd, false);
        let diffX = e.x - this.diffX;

        let diffY = e.y - this.diffY;
        if (typeof this.props.onDragEnd === 'function') {
            this.props.onDragEnd(e, {x: this.startX + diffX, y: this.startY + diffY});
        }
    };

    render() {
        return React.cloneElement(this.props.children, {ref: this.element});
    }
}

DragableItem.propTypes = {
    onDragStart: PropTypes.func,
    onDrag: PropTypes.func,
    onDragEnd: PropTypes.func,
    data: PropTypes.object,
};