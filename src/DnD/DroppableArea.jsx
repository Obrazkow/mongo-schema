import React from 'react';
import PropTypes from 'prop-types';


export default class DroppableArea extends React.Component {

    constructor(props) {
        super(props);

        this.element = React.createRef();
    }

    componentDidMount() {
        window.addEventListener('activateDroppableArea', this.activateDroppable, false);
    }

    componentWillUnmount() {
        this.deactivateDroppable();
        this.activateDroppable = null;
        this.deactivateDroppable = null;
        this.onDragLeave = null;
        this.onDragEnter = null;
        this.onDrop = null;
    }

    activateDroppable = (e) => {
        this.element.current.addEventListener('mouseenter', this.onDragEnter, false);
        this.element.current.addEventListener('mouseleave', this.onDragLeave, false);
        window.addEventListener('mouseup', this.deactivateDroppable, false);
    };

    deactivateDroppable = (e) => {
        this.element.current.removeEventListener('mouseenter', this.onDragEnter, false);
        this.element.current.removeEventListener('mouseleave', this.onDragLeave, false);
        window.removeEventListener('mouseup', this.deactivateDroppable, false);
        window.removeEventListener('dropitem', this.onDrop, false);
    };

    onDragLeave = e => {
        window.addEventListener('mouseup', this.deactivateDroppable, false);
        window.removeEventListener('dropitem', this.onDrop, false);
        if (typeof this.props.onDragLeave === 'function') {
            this.props.onDragLeave(e);
        }
    };

    onDragEnter = e => {
        window.removeEventListener('mouseup', this.deactivateDroppable, false);
        window.addEventListener('dropitem', this.onDrop, false);
        if (typeof this.props.onDragOver === 'function') {
            this.props.onDragOver(e);
        }
    };

    onDrop = (e) => {
        this.deactivateDroppable();
        if (typeof this.props.onDrop === 'function') {
            this.props.onDrop(e.data);
        }
    };

    render() {
        return React.cloneElement(this.props.children, {ref: this.element});
    }
}

DroppableArea.propTypes = {
    validSource: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    onDrop: PropTypes.func,
    onDragOver: PropTypes.func,
    onDragLeave: PropTypes.func,
};