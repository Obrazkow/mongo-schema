export const fetchPaths = (_items, _paths) => {
    if (!_items || !_paths) {
        return [];
    }

    return Object.values(_paths).map(path => {
        const fromNode = _items[path.from.node];
        const fromAttr = fromNode.attrs[path.from.attr];
        const from = {
            y: fromNode.y + (fromAttr.cy || 0),
            x: fromNode.x + (fromAttr.cx || 0),
            width: fromNode.width || 0
        };

        const toNode = _items[path.to.node];
        const toAttr = toNode.attrs[path.to.attr];
        const to = {
            y: toNode.y + (toAttr.cy || 0),
            x: toNode.x + (toAttr.cx || 0),
            width: toNode.width || 0
        };

        return {id: path.id, from: from, to: to}

    });
};