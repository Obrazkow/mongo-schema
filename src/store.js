import {createStore, combineReducers} from 'redux';

let initialState = {
    app: {
        items: {
            'dawd': {
                id: 'dawd',
                name: "TableA",
                x: 250,
                y: 150,
                attrs: {
                    "tt": {
                        id: 'tt',
                        label: 'Prop1',
                    },
                    "bb": {
                        id: 'bb',
                        label: 'Prop2',
                    },
                    "cc": {
                        id: 'cc',
                        label: 'Prop2123123123123123',
                    }
                }
            },
            'daww': {
                id: "daww",
                name: "TableB",
                x: 333,
                y: 250,
                attrs: {
                    "ytt": {
                        id: 'ytt',
                        label: 'Prop1',
                    },
                    "sss": {
                        id: 'sss',
                        label: 'Prop2',
                    }
                }
            }
        },
        paths: {
            '221_112': {
                id: '221_112',
                from: {
                    node: 'daww',
                    attr: 'ytt'
                },
                to: {
                    node: 'dawd',
                    attr: 'tt'
                }
            }
        },
        grid: {
            x: 0,
            y: 0
        }
    }
};

const app = (state = initialState, action) => {
    switch (action.type) {
        case 'ON_ITEM_UPDATE': {
            const item = state.items[action.payload.id];

            if (!item) {
                return state;
            }

            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: {
                        ...item,
                        ...action.payload
                    }
                }
            };
        }
        case 'ADD_ITEM': {
            const item = action.payload;

            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: item
                }
            };
        }
        case 'REMOVE_ITEM': {
            const item = state.items[action.payload.id];

            if (!item) {
                return state;
            }

            const {[item.id]: _, ...restItems} = state.items;

            let paths = {};
            Object.values(state.paths)
                .filter(path => path.from.node !== item.id && path.to.node !== item.id)
                .reduce((paths, path) => paths[path.id] = path, paths);

            return {...state, items: restItems, paths: paths};
        }
        case 'ADD_ATTR': {
            const attr = action.payload;

            const item = state.items[attr.nodeId];
            if (!item) {
                return state;
            }

            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: {
                        ...item,
                        attrs: {
                            ...item.attrs,
                            [attr.id]: attr
                        }
                    }
                }
            };
        }
        case 'ON_ATTR_UPDATE': {
            const attrData = action.payload.data;

            const item = state.items[action.payload.nodeId];
            if (!item) {
                return state;
            }

            const attr = item.attrs[attrData.id];
            if (!attr) {
                return state;
            }


            return {
                ...state,
                items: {
                    ...state.items,
                    [item.id]: {
                        ...item,
                        attrs: {
                            ...item.attrs,
                            [attr.id] : {
                                ...attr,
                                ...action.payload.data
                            }
                        }
                    }
                }
            };
        }
        case 'MOVE_GRID': {
            return {...state, grid: action.payload};
        }
        case 'ON_PATH_CONNECT':
            const connection = action.payload;
            const id = connection.from.node + connection.from.attr + '_' + connection.to.node + connection.to.attr;

            const path = state.paths[id];
            if (path) {
                return state;
            }

            return {
                ...state,
                paths: {
                    ...state.paths,
                    [id]: {
                        ...connection,
                        id: id
                    }
                }
            };
        case 'ON_PATH_DISCONNECT': {
            const path = state.paths[action.payload.id];

            if (!path) {
                return state;
            }

            const {[action.payload.id]: Id, ...rest} = state.paths;

            return {...state, paths: rest};
        }
        default:
            return state;
    }
};

const store = createStore(
    combineReducers({app}),
    initialState
);

export default store;

