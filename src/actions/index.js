export const onItemUpdate = (data) => ({
    type: 'ON_ITEM_UPDATE',
    payload: data
});

export const onDisconnect = (data) => ({
    type: 'ON_PATH_DISCONNECT',
    payload: data
});

export const onAttrUpdate = data => ({
    type: 'ON_ATTR_UPDATE',
    payload: data
});

export const onConnect = data => ({
    type: 'ON_PATH_CONNECT',
    payload: data
});

export const addAttr = data => ({
    type: 'ADD_ATTR',
    payload: data
});

export const onMoveGrid = data => ({
    type: 'MOVE_GRID',
    payload: data
});

export const onAddItem = data => ({
    type: 'ADD_ITEM',
    payload: data
});

export const onRemoveItem = data => ({
    type: 'REMOVE_ITEM',
    payload: data
});