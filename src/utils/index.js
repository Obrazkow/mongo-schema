export function generateId() {
    return Math.random().toString(32).substring(2, 15);
}