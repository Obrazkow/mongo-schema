import React from 'react';
import DroppableArea from "../../DnD/DroppableArea";
import AttributeType from "./part/AttributeType";
import AttributeLabel from "./part/AttributeLabel";
import AttributeRef from "./part/AttributeRef";

export default class AttributeView extends React.PureComponent {

    componentDidMount() {
        this.props.update({
            id: this.props.id,
            cx: 0,
            cy: this.props.offsetY + 10 + this.props.position * 20
        })
    }

    componentWillUnmount() {
        this.onConnect = null
    }

    onConnect = data => {
        if(data.nodeId !== this.props.nodeId) {
            this.props.connect({
                from: {
                    node: data.nodeId,
                    attr: data.attrId
                },
                to: {
                    node: this.props.nodeId,
                    attr: this.props.id
                }
            })
        }
    };

    render() {
        console.log(123);

        return (
            <DroppableArea key={this.props.id} onDragOver={this.onDragOver} onDragLeave={this.onDragLeave} onDrop={this.onConnect}>
                <svg id={this.props.id} x={this.props.x} y={this.props.position * 20} width={'100%'} height={400} ref={this.element}>
                    <AttributeLabel label={this.props.label}/>
                    <AttributeType type={'string'}/>
                    <AttributeRef id={this.props.id} nodeId={this.props.nodeId}/>
                </svg>
            </DroppableArea>
        )
    }
}