import React from 'react';

export default function AttributeLabel(props) {

    const style = {fontSize: '16px'};

    return <svg width={'60%'}><text style={style} y={'1em'}> {props.label} </text></svg>
}