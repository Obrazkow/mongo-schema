import React from 'react';

export default function AttributeType(props) {

    return <svg width={'30%'} x={'65%'}><text y={'1em'}> : {props.type} </text></svg>
}