import React from 'react';
import DraggableItem from "../../../DnD/DraggableItem";

export default function AttributeRef(props) {

    const data = {nodeId: props.nodeId, attrId: props.id};

    return <DraggableItem data={data}><circle cx={'95%'} cy={10} r={3}/></DraggableItem>
}