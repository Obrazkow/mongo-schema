import React from 'react';
import {connect} from "react-redux";
import {onAttrUpdate, onConnect} from "../../../actions";

class AttributeContainer extends React.Component {

    componentWillUnmount() {
        this.onConnect = null
    }

    connect = connection => {
        this.props.onConnect(connection);
    };

    update = attr => {
        this.props.onAttrUpdate({nodeId: this.props.nodeId, data: attr});
    };

    render() {
        const {view: View, ...rest} = this.props;

        return this.props.attrs
            .map((attr, elem) => (
                <View
                    { ...attr }
                    { ...rest }
                    key={attr.id}
                    update={this.update}
                    connect={this.connect}
                    position={elem}
                />
            ));
    }
}

export default connect(
    state => ({
        paths: state.app.paths
    }),
    dispatch => ({
        onConnect: data => dispatch(onConnect(data)),
        onAttrUpdate: data => dispatch(onAttrUpdate(data))
    })
)(AttributeContainer);