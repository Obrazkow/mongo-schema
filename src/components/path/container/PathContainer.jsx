import React from 'react';
import {connect} from "react-redux";
import {fetchPaths} from "../../../functions";
import {onDisconnect} from "../../../actions";

class PathContainer extends React.Component {

    disconnect = id => this.props.onDisconnect({id: id});

    render() {
        const {view: View} = this.props;

        return fetchPaths(this.props.items, this.props.paths)
            .map(path =>
                <View
                    key={path.id}
                    id={path.id}
                    from={path.from}
                    to={path.to}
                />
            );
    }
}

export default connect(
    state => ({
        items: state.app.items,
        paths: state.app.paths
    }),
    dispatch => ({
        onDisconnect: data => dispatch(onDisconnect(data))
    })
)(PathContainer)