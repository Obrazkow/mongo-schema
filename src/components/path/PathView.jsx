import React from 'react';

export default class PathView extends React.Component {

    shouldComponentUpdate(nextProps) {
        if (this.props.id !== nextProps.id) {
            return true;
        }

        const {from, to} = this.props;

        if (from.x !== nextProps.from.x || from.y !== nextProps.from.y || from.width !== nextProps.from.width) {
            return true;
        }

        return to.x !== nextProps.to.x || to.y !== nextProps.to.y || to.width !== nextProps.to.width
    }

    onDisconnect = () => {
        if (confirm('Are you sure?')) {
            this.props.disconnect(this.props.id);
        }
    };

    render() {
        let {from, to} = this.props;

        if (from.x > to.x) {
            [from, to] = [to, from];
        }

        let x1 = from.width + from.x;
        let x2 = to.x;

        let cxFix = 0;
        if (x2 - x1 <= 20) {
            x2 += to.width;
            cxFix += to.width;
        }
        let cx1 = cxFix + x1 + (x2 - x1) / 2;
        let cx2 = cxFix + x2 - (x2 - x1) / 2;

        const path = `M${x1} ${from.y} C ${cx1} ${from.y}, ${cx2} ${to.y}, ${x2} ${to.y}`;

        return <path id={this.props.id} onClick={this.onDisconnect} d={path} strokeWidth={3} fill={'none'} stroke={'black'}/>;
    }
}