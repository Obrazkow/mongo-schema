import React from 'react';
import PathView from "../path/PathView";
import {connect} from "react-redux";
import {onMoveGrid} from "../../actions";
import DragableItem from "../../DnD/DraggableItem";
import Board from "../board/Board";

import './style.less'
import ItemsContainer from "../item/container/ItemsContainer";
import ItemView from "../item/ItemView";
import PathContainer from "../path/container/PathContainer";

class Grid extends React.Component {

    componentWillUnmount() {
        this.renderPath = null;
        this.onDrag = null;
    }

    onDrag = (e, pos) => {
        this.props.onMoveGrid({x: pos.x > 0 ? 0 : pos.x, y: pos.y > 0 ? 0 : pos.y});
    };


    render() {
        const {x, y} = this.props.grid;

        return (
            <div className={'grid-schema-wrap'}>
                <Board startNodeY={-y} startNodeX={-x}/>
                <div className={'grid-schema'} style={{top: y, left: x}}>
                    <DragableItem onDrag={this.onDrag}>
                        <svg className={'grid-schema svg'} id={'schema'} x={x} y={y}>
                            <PathContainer key={1} view={PathView}/>
                            <ItemsContainer view={ItemView}/>
                        </svg>
                    </DragableItem>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        grid: state.app.grid
    }),
    dispatch => ({
        onMoveGrid: data => dispatch(onMoveGrid(data)),
    })
)(Grid);