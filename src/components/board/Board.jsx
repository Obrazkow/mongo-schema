import React from 'react';
import connect from "react-redux/es/connect/connect";
import { onAddItem } from "../../actions";
import PropTypes from "prop-types";
import {generateId} from "../../utils/index";

import './style.less'

class Board extends React.Component {


    onAddItem = () => {
        this.props.onAddItem({
            id: generateId(),
            attrs: {},
            name: prompt('name of node'),
            x: -this.props.startNodeX,
            y: -this.props.startNodeY
        })
    };

    render() {
        return (
            <div className={"board"}>
                <div className={'container'} >
                    <button
                        onClick={this.onAddItem}
                        className={'btn btn-default'}
                    >Add Node</button>
                </div>
            </div>
        )
    }
}

Board.propTypes = {
    startNodeX: PropTypes.number.isRequired,
    startNodeY: PropTypes.number.isRequired
};

export default connect(
    null,
    dispatch => ({
        onAddItem: data => dispatch(onAddItem(data))
    })
)(Board);