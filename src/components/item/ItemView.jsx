import React from 'react';
import DragableItem from "../../DnD/DraggableItem";
import {ItemHeader} from "./part/ItemHeader";
import {ItemBody} from "./part/ItemBody";
import ItemFooter from "./part/ItemFooter";

const ITEM_WIDTH = 200;

export default class ItemView extends React.Component {

    componentDidMount() {
        this.props.update(this.props.id, {width: ITEM_WIDTH})
        this.schema = document.getElementById('schema');
    }

    componentWillUnmount() {
        this.onDrag = null;
        this.remove = null;
    }

    onDragStart = () => {
        this.schema.appendChild(document.getElementById(this.props.id));
    };

    onDrag = (e, position) => {
        if (position.x === parseInt(position.x) && position.y === parseInt(position.y)) {
            this.props.update(this.props.id, position)
        }
    };

    remove = () => this.props.remove(this.props.id);

    render() {
        let elemHeight = 20;

        const style = {userSelect: 'none'};
        const attrs = Object.values(this.props.attrs)
        const bodyHeight = attrs.length * elemHeight;

        return (
            <DragableItem onDrag={this.onDrag} onDragStart={this.onDragStart}>
                <svg id={this.props.id} x={this.props.x} y={this.props.y} width={ITEM_WIDTH} style={style}>
                    <svg height={elemHeight} width={'100%'}>
                        <ItemHeader name={this.props.name} remove={this.remove}/>
                    </svg>
                    <svg y={elemHeight} height={bodyHeight} width={'100%'}>
                        <ItemBody attrs={attrs} nodeId={this.props.id} offsetY={elemHeight}/>
                    </svg>
                    <svg y={elemHeight + bodyHeight} height={elemHeight} width={'100%'}>
                        <ItemFooter
                            nodeId={this.props.id}
                            addAttribute={this.props.addAttribute}
                        />
                    </svg>
                </svg>
            </DragableItem>
        )
    }
}