import React from 'react';
import {connect} from "react-redux";
import {addAttr, onItemUpdate, onRemoveItem} from "../../../actions";

class ItemsContainer extends React.Component {

    componentDidMount() {
        this.props.onItemUpdate({id: this.props.id, width: 200})
    }

    componentWillUnmount() {
        this.update = null;
        this.remove = null;
        this.addAttribute = null
    }

    update = (id, data) => this.props.onItemUpdate({id: id, ...data});

    remove = id => this.props.removeItem({id: id});

    addAttribute = (nodeId, label)=> {
        this.props.addAttribute({nodeId: nodeId, id: Math.random().toString(36).substring(2, 15), label});
    };

    render() {
        const {view: View} = this.props;

        return Object.values(this.props.items)
            .map(itemData =>
                <View
                    { ...itemData }
                    key={itemData.id}
                    update={this.update}
                    remove={this.remove}
                    addAttribute={this.addAttribute}
                />
            );
    }
}

export default connect(
    state => ({
        items: state.app.items
    }),
    dispatch => ({
        onItemUpdate: data => dispatch(onItemUpdate(data)),
        removeItem: data => dispatch(onRemoveItem(data)),
        addAttribute: data => dispatch(addAttr(data))
    })
)(ItemsContainer)