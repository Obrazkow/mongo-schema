import React from 'react';

export function ItemHeader(props) {
    return (
        <React.Fragment>
            <rect fill={'green'} width={'100%'}/>
            <text y={'1em'} x={5}>{props.name}</text>
            <text y={'1em'} x={'90%'} onClick={props.remove}>x</text>
        </React.Fragment>
    )
}