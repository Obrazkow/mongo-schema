import React from 'react'

export default function ItemFooter(props) {

    const addAttribute = () => {
        props.addAttribute(props.nodeId, prompt('Attribute name'))
    };

    return (
        <React.Fragment>
            <rect fill={'blue'} height={'100%'} width={'100%'}/>
            <text y={'1em'} onClick={addAttribute}>AddItem</text>
        </React.Fragment>
    )

}