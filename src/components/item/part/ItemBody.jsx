import React from 'react';
import AttributeContainer from "../../attr/container/AttributeContainer";
import AttributeView from "../../attr/AttributeView";

export function ItemBody(props) {

    return (
        <React.Fragment>
            <rect height={'100%'} fill={'orange'} width={'100%'}/>
            <AttributeContainer {...props} view={AttributeView}/>
        </React.Fragment>
    )
}